from django.apps import AppConfig


class MdmappConfig(AppConfig):
    name = 'MDMapp'

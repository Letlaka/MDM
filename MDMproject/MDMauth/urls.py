from django.urls import path, include
from . import views

urlpatterns = [
    path('profile/', views.profile, name = 'profile'),
    path('', include('MDMapp.urls')),
    path('', views.login, name="login"),
    path('register/', views.register, name='register'),
]
